"use strict";
const { useState } = React;
/**
 * React functional component representing the main application.
 * @function App
 * @returns {React.Fragment} - The main application component.
 */
function App() {
    const [currentTyped, setCurrentTyped] = useState("");
    const [recentResults, setRecentResults] = useState(null);
    const [currentViewed, setCurrentViewed] = useState("");
     /**
     * Handles the click event on an artwork item, fetching and updating the viewed artwork details.
     * @function handleItemClick
     * @param {string} artworkId - The ID of the clicked artwork.
     */
    const handleItemClick = (artworkId) => {
        fetch(`https://api.artic.edu/api/v1/artworks/${artworkId}`)
            .then((response) => {
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }
                return response.json();
            })
            .then((artworkDetails) => {
                setCurrentViewed(artworkDetails.data);
            })
            .catch((error) => {
                setError(error.message);
            });
    };
    return (
        <React.Fragment>
            <SingleList
                currentTyped={currentTyped}
                recentResults={recentResults}
                setCurrentTyped={setCurrentTyped}
                setRecentResults={setRecentResults}
                handleItemClick={handleItemClick}
            />
            <Details currentViewed={currentViewed} />
        </React.Fragment>
    );
}
function SingleList({ currentTyped, recentResults, setCurrentTyped, setRecentResults, handleItemClick }) {
    const [error, setError] = useState(null);
    const [selectedIndex, setSelectedIndex] = useState(-1);

    /**
     * Handles the form submission for searching artworks.
     * @function handleSubmit
     * @param {Event} e - The form submission event.
     */
    const handleSubmit = (e) => {
        e.preventDefault();
        fetch(`https://api.artic.edu/api/v1/artworks/search?q=${currentTyped}`)
        .then((response) => response.json())
        .then((data) => setRecentResults(data))
        .catch((error) => setError(error));
    };
    return (
        <header>
            <h1>Art Institute of Chicago Search</h1>
            <form onSubmit={handleSubmit}>
                <input
                    id="textInput"
                    type="text"
                    value={currentTyped}
                    onChange={(e) => setCurrentTyped(e.target.value)}
                />
                <button type="submit">Submit</button>
            </form>
            <ul>
                {error && <li key="error">{error + ""}</li>}
                {recentResults &&
                    recentResults.data.map((result, index) => (
                        <li className={selectedIndex === index ? 'active-li' : 'li-class'} key={result.id} onClick={() => { handleItemClick(result.id); setSelectedIndex(index); }}>
                            <strong>Title:</strong> {result.title}<br />
                        </li>
                    ))}
            </ul>
        </header>
    );
}
/**
 * React functional component representing the details view.
 * @function Details
 * @param {object} props - The component props.
 * @param {object} props.currentViewed - The currently viewed artwork details.
 * @returns {JSX.Element} - The details view component.
 */
function Details({ currentViewed }) {
    return (
        <main>
            {currentViewed ? (
                <div>
                    <h2>{currentViewed.title}</h2>
                    <div className="artist-info">{currentViewed.artist_display}</div>
                    <img
                        src={`https://www.artic.edu/iiif/2/${currentViewed.image_id}/full/843,/0/default.jpg`}
                        alt={"No image found..."}
                    />
                    <p dangerouslySetInnerHTML={{ __html: currentViewed.description }} />
                    <dl>
                        {currentViewed.medium_display && (
                            <React.Fragment>
                                <dt>Medium:</dt>
                                <dd>{currentViewed.medium_display}</dd>
                            </React.Fragment>
                        )}
                        {currentViewed.dimensions && (
                            <React.Fragment>
                                <dt>Dimensions:</dt>
                                <dd>{currentViewed.dimensions}</dd>
                            </React.Fragment>
                        )}
                    </dl>
                </div>
            ) : (
                <p>No item selected</p>
            )}
        </main>
    );
}
// Render the main application component into the specified HTML element
ReactDOM.render(<App />, document.querySelector('#react'));
